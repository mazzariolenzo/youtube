package practica1_uf4;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Canal {
	Scanner leer = new Scanner(System.in);
	public static Video videoActual; // almacena el video seleccionado

	private String nombre;
	private Date fechaCreacion;
	protected ArrayList<Video> videos;

	public Canal(String nombre) {
		setNombre(nombre);
		setFechaCreacion(new Date());
		videos = new ArrayList<>();
	}

	// setters y getters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setFechaCreacion(Date fecha) {
		this.fechaCreacion = fecha;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void menuCanal(Scanner leer) {
		System.out.println("|---" + Youtube.canalActual.getNombre() + "---| \n 1-Nuevo video \n 2-Seleccionar video \n"
				+ " 3-Mostrar estadisticas \n 4-Mostrar info videos \n 0-Salir");

		int opcion = leer.nextInt();
		leer.nextLine();

		switch (opcion) {
		case 1:
			creaVideo(leer);
			break;
		case 2:
			seleccionarVideo(leer);
			break;
		case 3:
			mostrarEstadisticas();
			break;
		case 4:
			mostrarInfoVideos(leer);
			break;
		case 0:
			Youtube.mostrarMenu(leer);
			break;
		default:
			System.out.println("Error! Ingresa un numero del 0 al 4");
			break;

		}
	}

	public void infoCanal() {
		System.out.println("Nombre del canal: " + getNombre() + ", creado en fecha " + getFechaCreacion() + ", con "
				+ videos.size() + " videos.");
	}

	public void creaVideo(Scanner leer) {

		System.out.println("Introduce el titulo de tu nuevo video:");
		String nuevoTitulo = leer.nextLine();
		System.out.println();

		Video nuevoVideo = new Video(nuevoTitulo);
		videos.add(nuevoVideo);

		System.out.println("Video subido correctamente!");

		videoActual = nuevoVideo;
		// muestra canal creado como seleccionado
		System.out.println("Video seleccionado: " + videoActual.getTitulo());
		System.out.println();

		nuevoVideo.menuVideo(leer);
	}

	public void seleccionarVideo(Scanner leer) {
		if (videos.isEmpty()) {
			System.out.println("No hay videos para seleccionar \n Sube un nuevo video");
			System.out.println();
			creaVideo(leer);

		} else {
			System.out.println("Ingresa el numero del video que desea seleccionar:");
			System.out.println();

			// Muestra el arrayList con los videos y su info
			for (int i = 0; i < videos.size(); i++) {
				System.out.print(i + 1 + "-");
				videos.get(i).infoVideo();
				System.out.println();
			}
		}

		int opcionVideo = leer.nextInt();

		if (opcionVideo >= 1 && opcionVideo <= videos.size()) {
			videoActual = videos.get(opcionVideo - 1);
			System.out.println("Canal seleccionado: " + videoActual.getTitulo());
			System.out.println();
			videoActual.menuVideo(leer); // Mostrar el menú del canal seleccionado
		} else {
			System.out.println("Error: Índice de canal no válido.");
			videoActual.menuVideo(leer);
		}

		// videoActual.menuVideo(leer);

	}

	public String toStringCanal() {
		return "Nombre del canal: '" + nombre + "' creado en fecha: " + fechaCreacion + ", con  " + videos.size()
				+ " videos";

	}

	public void mostrarEstadisticas() {

		if (Youtube.canalActual != null) {
			System.out.println("Estadísticas del canal actual:");

			// funcion toString para obtener estadisticas
			System.out.println(Youtube.canalActual.toStringCanal());
			System.out.println();
		} else {
			System.out.println("No se ha seleccionado ningún canal.");
		}

		// volver al menu del canal
		Youtube.canalActual.menuCanal(leer);
	}

	public void mostrarInfoVideos(Scanner leer) {

		if (videos.isEmpty()) {
			System.out.println("Aun no hay videos disponibles.");
			System.out.println();

			Youtube.canalActual.menuCanal(leer);
		}
		System.out.println("Videos del canal:");
		for (int i = 0; i < videos.size(); i++) {
			Video video = videos.get(i);
			System.out
					.println((i + 1) + ": '" + video.getTitulo() + "' en fecha " + video.getFechaCreacion(fechaCreacion)
							+ " con " + video.getNumLike() + " likes y " + video.comentarios.size() + " comentarios");
		}

	}
}