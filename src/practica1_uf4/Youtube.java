package practica1_uf4;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Youtube {
	protected static ArrayList<Canal> canales = new ArrayList<>();
	public static Canal canalActual; // almacena el canal seleccionado

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		int opcion = 0;

		do {
			opcion = mostrarMenu(leer);
      leer.nextLine();

			switch (opcion) {
			case 1:
				creaCanal(leer);
				break;
			case 2:
				seleccionarCanal(leer);
				break;
			case 3:
				mostrarEstadisticas();
				break;
			case 4:
				mostrarEstadisticasCompletas(null);
				break;
			case 0:
				System.out.println("Gracias! Programa hecho por Enzo Mazzariol, alumno de DAM ");
				break;
			default:
				System.out.println("Error! ingresa un numero del 0 al 4");
				break;
			}

			System.out.println();
			System.out.println();
		} while (opcion != 0);
	}

	public static int mostrarMenu(Scanner leer) {
		int opcion;
		System.out.println("|---YOUTUBE---| \n 1-Nuevo canal \n 2-Seleccionar Canal \n "
				+ "3-Mostrar estadisticas \n 4-Mostrar estadisticas completas \n 0-Salir \n"
				+ "|---------------------|");
		return leer.nextInt();
	}

	public static void creaCanal(Scanner leer) {

		// datos para crear nuevo canal
		System.out.println("Introduce el nombre de tu canal:");
		String nuevoNombre = leer.nextLine();
		System.out.println();

		Canal nuevoCanal = new Canal(nuevoNombre);

		canales.add(nuevoCanal);

		System.out.println("Canal registrado correctamente!");

		canalActual = nuevoCanal;
		System.out.println("Canal seleccionado: " + canalActual.getNombre());
		System.out.println();

		nuevoCanal.menuCanal(leer);

	}

	public static void seleccionarCanal(Scanner leer) {
		if (canales.isEmpty()) {
			//manda a usuario a crea canal si el arrayList esta vacio
			System.out.println("No hay canales para seleccionar \n Crea un nuevo canal!");
			System.out.println();
			creaCanal(leer);
			return;
		}

		System.out.println("Ingrese el número del canal que desea seleccionar:");
		System.out.println();

		//Muestra canales disponibles y su info
		for (int i = 0; i < canales.size(); i++) {
			System.out.print(i + 1 + ")");
			canales.get(i).infoCanal();
			System.out.println();
		}

		int opcionCanal = leer.nextInt();

		// Verificar si la opción seleccionada es válida
		if (opcionCanal >= 1 && opcionCanal <= canales.size()) {
			canalActual = canales.get(opcionCanal - 1);
			System.out.println("Canal seleccionado: " + canalActual.getNombre());
			System.out.println();
			canalActual.menuCanal(leer); // Mostrar el menú del canal seleccionado
		} else {
			System.out.println("Error: Índice de canal no válido.");
			System.out.println();
		}

		//Vuelve al menu
		canalActual.menuCanal(leer);

	}

	public static void mostrarEstadisticas() {
		if (canales.size() == 1) {
			System.out.println("Youtube tiene 1 canal");
		} else {
			System.out.println("Youtube tiene " + canales.size() + " canales");
		}
	}

	public static void mostrarEstadisticasCompletas(Date Date) {
		 if (canales.isEmpty()) {
		        System.out.println("No hay canales para mostrar estadísticas completas.");
		        return;
		    }

		if (canales.size() == 1) {
			System.out.println("Youtube tiene 1 canal");
		} else {
			System.out.println("Youtube tiene " + canales.size() + " canales");
		}


		// Estadisticas de todos los canales
		for (int i = 0; i < canales.size(); i++) {
			
			Canal canal = canales.get(i);	
			System.out.println("-Nombre del canal: '" + canal.getNombre() + "' creado en fecha:"
					+ canal.getFechaCreacion() + " con " + canal.videos.size() + " videos");

			for (int j = 0; j < canal.videos.size(); j++) {
				Video video = canal.videos.get(j);
				System.out.println("---Título del video: " + video.getTitulo() + ", creado en fecha: "
						+ video.getFechaCreacion(null) + ", con " + video.getNumLike() + " likes");

				for (int k = 0; k < video.comentarios.size(); k++) {
					Comentario comentario = video.comentarios.get(k);
					System.out.println("-----Usuario: " + comentario.getTexto() + ", Comentario: "
							+ comentario.getUsuario() + ", Fecha de creación: " + 
							comentario.getFechaCreacion(null));
				}

			}
		}
		
	}
}