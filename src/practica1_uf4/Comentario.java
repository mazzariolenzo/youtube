package practica1_uf4;

import java.util.Date;

public class Comentario {

	private String usuario;
	private String texto;
	private Date fechaCreacion;

	public Comentario(String nuevoUsuario, String texto) {
		this.usuario = nuevoUsuario;
		this.texto = texto;
		this.fechaCreacion = new Date();
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setFechaCreacion(Date fecha) {
		this.fechaCreacion = fecha;
	}

	public Date getFechaCreacion(Date fecha) {
		return fechaCreacion;
	}
	
	
}