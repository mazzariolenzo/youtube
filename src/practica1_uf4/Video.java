package practica1_uf4;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Video {

	Scanner leer = new Scanner(System.in);

	public static Comentario comentarioActual;
	private String titulo;
	private int numLike;
	private Date fechaCreacion;
	protected ArrayList<Comentario> comentarios;

	public Video(String titulo) {
		this.titulo = titulo;
		this.numLike = numLike;
		fechaCreacion = new Date();
		comentarios = new ArrayList<>();
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setFechaCreacion(Date fecha) {
		fechaCreacion = fecha;
	}

	public Date getFechaCreacion(Date fecha) {
		return fechaCreacion;
	}

	public void setNumLike(int numLike) {
		this.numLike = numLike;
	}

	public int getNumLike() {
		return numLike;
	}

	public void menuVideo(Scanner leer) {
		System.out.println("|---" + Canal.videoActual.getTitulo() + "---| \n 1-Nuevo comentario \n 2-Like \n"
				+ "3-Mostrar comentarios \n 4-Mostrar estadisticas \n 0-Salir");

		int opcion = leer.nextInt();
		leer.nextLine();

		switch (opcion) {
		case 1:
			creaComentario(leer);
			break;
		case 2:
			darLike(numLike, leer);
			break;
		case 3:
			mostrarComentarios(leer);
			break;
		case 4:
			mostrarEstadisticas();
			break;
		case 0:
			Youtube.canalActual.menuCanal(leer);
			return;
		default:
			System.out.println("Error! Ingresa un numero del 0 al 4");
			break;

		}
	}

	public void infoVideo() {
		System.out.println("Video: '" + getTitulo() + "', en fecha " + getFechaCreacion(fechaCreacion) + ", con "
				+ getNumLike() + " likes y " + comentarios.size() + " comentarios");
	}

	public void creaComentario(Scanner leer) {

		System.out.println("Introduce tu nombre de usuario:");
		String nuevoUsuario = leer.nextLine();

		System.out.println("Introduce comentario:");
		String texto = leer.nextLine();
		System.out.println();

		Comentario nuevoComentarios = new Comentario(texto, nuevoUsuario);
		comentarios.add(nuevoComentarios);

		System.out.println("Comentario subido correctamente!");
		System.out.println();

		Youtube.canalActual.menuCanal(leer);

	}

	public void darLike(int numLike, Scanner leer) {
		this.numLike++;
		System.out.println("\u001B[31mME GUSTA\u001B[0m");

		Youtube.canalActual.menuCanal(leer);

	}

	public void like(int numLike) {
		numLike++;
	}

	public void mostrarComentarios(Scanner leer) {
		if (comentarios.isEmpty()) {
			System.out.println("Aun no hay comentarios disponibles");
			System.out.println();

			Youtube.canalActual.menuCanal(leer);
		}

		for (int i = 0; i < comentarios.size(); i++) {
			System.out.print(i + 1 + "-Comentario: " + "'" + comentarios.get(i).getTexto() + "'" + " del usuario: "
					+ comentarios.get(i).getUsuario() + " en fecha "
					+ comentarios.get(i).getFechaCreacion(fechaCreacion));

			System.out.println();
		}
	}

	public void mostrarEstadisticas() {

		System.out.println("Video: '" + getTitulo() + "', en fecha " + getFechaCreacion(fechaCreacion) + ", con "
				+ getNumLike() + " likes y " + comentarios.size() + " comentarios");
		System.out.println();

		/*
		 * for (int i = 0; i < comentarios.size(); i++) { System.out.print(i + 1 +
		 * "-Video: " + "'" + comentarios.get(i) + "'" + " en fecha " +
		 * getFechaCreacion(fechaCreacion) + " con " + getNumLike() + " likes y " +
		 * comentarios.size() + " comentarios");
		 * 
		 * System.out.println(); }
		 */
	}

	public String toStringVideos() {
		return "Video: '" + getTitulo() + "', en fecha " + getFechaCreacion(fechaCreacion) + ", con " + getNumLike()
				+ " likes y " + comentarios.size() + " comentarios";

	}
}